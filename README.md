# SRH Logger

This tool logs in and clicks for you in a certain HR tool whose name starts
with S. It also displays your current working time and whether you're currently
working or not according to the system. Currently only certain company that
uses SRH is supported.

The software is provided as is, without any guarantees, and the developers
decline any responsibility from its usage. Please comply with the security
policies of your company.

## Rationale

SRH has a clunky interface even with the recent simplifications, and it's
insanely slow with a poor user experience. Before it wasn't an issue, but when
you need to click a button +4 times per day, it gets old quick.  Until the poor
UX/UI gets fixed, I will use this, and share it with those who find it useful.

## Installation

This software requires the installation of chrome/chromium and chromedriver to
work. On a Debian/Ubuntu this usually involves:

    sudo apt-get install chromium-browser chromium-chromedriver

After that dependency, you can install this program from source:

    sudo python3 setup.py install

For a system-wide installation.

Alternatively you can install it with within your local/virtual environment with:

    pip3 install -e .

## Usage

The tool displays how to use it with the --help flag.

    Usage: srh-logger [OPTIONS]
    
    Options:
      -u, --user TEXT                 username, can be set as env var SRH_USER
                                      [required]
      -p, --password TEXT             password, can be set as env var
                                      SRH_PASSWORD  [required]
      -l, --list-clockings / --no-list-clockings
                                      list current clockings  [default:
                                      (activated)]
      -c, --clock / --no-clock        clock the current time (i.e. click the button)
                                      [default: (deactivated)]
      -q, --headless / --no-headless  headless operation  [default: (activated)]
      -s, --site TEXT                 main URI to log in, can be set as env var
                                      SRH_SITE [required]
      -t, --timeout INTEGER           time limit for each browser operation
                                      [default: 120]
      -r, --retry INTEGER             number of retries when reaching timeout
                                      [default: 3]
      --help                          Show this message and exit.



    ➤  srh-logger --no-headless -c -t 100 -r 5
    2021-12-03T09:39:36+0100 [INFO] Reached https://[your srh site]
    2021-12-03T09:39:36+0100 [INFO] Logging in...
    2021-12-03T09:39:44+0100 [INFO] Pinging Voyager 1
    2021-12-03T09:39:54+0100 [INFO] Initializing coffee machine
    2021-12-03T09:40:04+0100 [INFO] Initializing 3D engine
    2021-12-03T09:40:14+0100 [INFO] Randomly pushing buttons
    2021-12-03T09:40:24+0100 [INFO] Logged in successfully
    2021-12-03T09:40:24+0100 [INFO] Clicking the button...
    2021-12-03T09:40:24+0100 [INFO] Waiting for paint wetness level to drop
    2021-12-03T09:40:25+0100 [INFO] Waiting for clocking to appear...
    2021-12-03T09:40:27+0100 [INFO] Clocking success!
    
    Type    Time      Activity                Origin
    ------  --------  ----------------------  --------
    Brut    09:40:25  [?en] Présence normale  $virtual
    Brut    08:39:25  [?en] Présence normale  $virtual
    Brut    08:30:26  [?en] Présence normale  $virtual
    Brut    07:39:20  [?en] Présence normale  $virtual
    
    You're NOT working
    Current logged time: 01h52m06s
    Total time saved by srh-logger: 00h00m55s

## FAQ

### Why this?

I don't like wasting my time, and most people required to do this don't either.

### How can I trust you?

You can't, please look very hard at the code before trusting it with your credentials.

### Could you please add automatic clockings?

No, this tool is only intended for manual reporting. Automatic reporting would
be fraud. If you want to commit fraud you'll have to do it on your own. I won't
accept merge requests with this functionality either, so don't bother.

### Could you support Windows?

No, but merge requests in that direction are welcome.

### Could you please add something else?

Perhaps, please open an issue or send me a message to discuss.


## License

GPLv3
