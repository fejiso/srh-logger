import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="srh-logger",
    version="0.0.1",
    author="Fernando Jiménez",
    author_email="srh-logger@fer.xyz",
    description="Tool that automates work time reporting on SRH HR systems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fejiso/srh-logger",
    project_urls={
        "git": "https://gitlab.com/fejiso/srh-logger",
        "Bug Tracker": "https://gitlab.com/fejiso/srh-logger/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Unix-like",
    ],
    entry_points = {
        'console_scripts': ['srh-logger=srh.srh:srh_action']
    },
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires = [ "selenium>=4.0.0,<5", "tabulate>=0.8.0,<1", "click>=8.0.0,<9","tenacity>=8.0.0,<9", "timeout-decorator>=0.5.0,<1", "cryptography>=33.0.0,<37", "setuptools-rust>=1.0.0"],
)
