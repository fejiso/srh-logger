#!/usr/bin/env python3

import os
import threading
import datetime
import click
import timeout_decorator
from tenacity import retry, retry_if_exception_type, stop_after_attempt, after_log
import logging
import random
import time
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from tabulate import tabulate

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%Y-%m-%dT%H:%M:%S%z',handlers=[logging.StreamHandler()])
logger = logging.getLogger("")
logger.setLevel(logging.INFO)

global_timeout = None
global_retries = None

class SRHError(Exception):
    pass

def printdelta(message, delta):
        hours, remainder = divmod(delta.total_seconds(),3600)
        minutes, seconds = divmod(remainder,60)
        print(message+ "{:02}h{:02}m{:02}s".format(int(hours),int(minutes), int(seconds)))

def timerdecorator(func):
    def wrapper(*args,**kwargs):
        try:
            args[0]._waiting = True
            result = func(*args,**kwargs)
        finally:
            args[0]._waiting = False

        return result
    return wrapper




class SRH_Logger():
    def __init__(self,username,password,headless):
        self.username = username
        self.password = password
        self.headless = headless
        options = webdriver.ChromeOptions()
        if headless:
            options.add_argument("headless")
        options.add_argument("--disable-gpu")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-extensions")
        self.browser = webdriver.Chrome(options=options)
        self.browser.set_window_size(4000,4000) # srh decides whether to load or not depending on the viewport...
        self.messages = ["Reticulating splines",
             "Initializing 3D engine",
             "Fermentation started",
             "Hitting elevator button",
             "Encapsulating HTTPS request in IPoAC",
             "Waiting for the Sun, Phobos and Charon to align",
             "Initializing coffee machine",
             "Solving the traveling salesman problem",
             "Awaiting HTTPS response via La Poste",
             "Waiting for paint wetness level to drop",
             "Polling height of grass",
             "Randomly pushing buttons",
             "Dropping all tables and starting from scratch",
             "Approaching heat death of the universe",
             "Pinging Voyager 1",
             "Snail race started, polling for results",
             "TCP checksum offloading is on strike, calculating checksums",
             "Launching DDoS against other users so our requests are prioritized",
             ]
        random.shuffle(self.messages)
        self._finished = False
        self._waiting = False
        self.task = threading.Thread(target=self.timer)
        self.task.start()

    def end(self):
        self.browser.quit()
        self._finished = True
        self.task.join()

    def timer(self):
        count = 0
        while not self._finished:
            count = count + 1 
            if self._waiting and count % 100 == 0:
                logger.info(self.messages[0])
                self.messages.append(self.messages[0])
                del self.messages[0]
            time.sleep(0.1)

    @retry(retry=retry_if_exception_type(timeout_decorator.TimeoutError),stop=stop_after_attempt(global_retries),after=after_log(logger,logging.INFO))
    @timeout_decorator.timeout(global_timeout)
    @timerdecorator
    def delay(self,delay):
        time.sleep(delay)
        return self

    @retry(retry=retry_if_exception_type(timeout_decorator.TimeoutError),stop=stop_after_attempt(global_retries),after=after_log(logger,logging.INFO))
    @timeout_decorator.timeout(global_timeout)
    @timerdecorator
    def go_to_url(self,site):
        self.browser.get(site)
        logger.info(f'Reached {site}')
        return self

        
    @retry(retry=retry_if_exception_type(timeout_decorator.TimeoutError),stop=stop_after_attempt(global_retries),after=after_log(logger,logging.INFO))
    @timeout_decorator.timeout(global_timeout)
    @timerdecorator
    def clock(self):
        logger.info("Clicking the button...")
        clockings = self.getClockings()
        self.browser.find_elements(By.NAME,"pointer")[0].click()
        while self.getClockings() == clockings:
            logger.info("Waiting for clocking to appear...")
            #self.screenshot()
            time.sleep(1)
        logger.info("Clocking success!")
        return self
        
    @retry(retry=retry_if_exception_type(timeout_decorator.TimeoutError),stop=stop_after_attempt(global_retries),after=after_log(logger,logging.INFO))
    @timeout_decorator.timeout(global_timeout)
    @timerdecorator
    def login(self):
        logger.info("Logging in...")
        user = []
        password = []
        submit = []
        while len(user) == 0 or len(password) == 0 or len(submit) == 0:
            user = self.browser.find_elements(By.ID,"userNameInput")
            password =self.browser.find_elements(By.ID,"passwordInput")
            submit = self.browser.find_elements(By.ID,"submitButton")
        user[0].send_keys(self.username)
        password[0].send_keys(self.password)
        submit[0].click()
        while True:
            errors  = self.browser.find_elements(By.ID,"error")
            if len (errors) > 0:
                msg = "Error logging in. Check user/pw. Exiting."
                logger.error(msg)
                raise SRHError(msg)
            time.sleep(1)
            clicker = self.browser.find_elements(By.NAME,"pointer")
            if len(clicker) > 0:
                break
        if self.getClockings() == []:
            time.sleep(3)
        logger.info(f"Logged in successfully")
        return self

    @timerdecorator
    def printClockings(self):
        timeformat = "%H:%M:%S"
        table = [ [c['type'],c['time'],c['activity'],c['origin']] for c in self.getClockings() ]
        print("\n"+tabulate(table,headers=["Type","Time","Activity","Origin"]))
        table.reverse()
        if len(table) % 2 == 0:
            print("\nYou're NOT working")
        else:
            print("\nYou ARE working")
            table.append(["cur",datetime.datetime.now().strftime(timeformat),"act","orig"])

        final_delta = datetime.timedelta(seconds=0)
        for i in range(0,len(table),2):
            start = datetime.datetime.strptime(table[i][1],timeformat)
            start = datetime.timedelta(hours=start.hour, minutes=start.minute, seconds=start.second)
            end = datetime.datetime.strptime(table[i+1][1],timeformat)
            end = datetime.timedelta(hours=end.hour, minutes=end.minute, seconds=end.second)
            final_delta = final_delta + end - start
        printdelta("Currently logged time: ", final_delta)
        return self

    @timerdecorator
    def screenshot(self):
        path = datetime.datetime.now().isoformat()+".png"
        body = self.browser.find_element(By.TAG_NAME,'body')
        body.screenshot(path)
        return self

    @timerdecorator
    def getClockings(self):
        while True:
            try:
                cells = self.browser.find_elements(By.CLASS_NAME,"jqx-grid-cell")
                logger.debug(f'Found {len(cells)} cells')
                logger.debug(f'Raw cell content {[ x.text for x in cells ]}')
                clockings = []
                if len(cells) % 6 != 0:
                    msg = "Unexpected number of cells, exiting"
                    logger.error(msg)
                    raise SRHError(msg)
                clocking_index = 0
                for i in range(0,len(cells),6):
                    clocking_type = cells[i+0].text
                    clocking_time = cells[i+1].text
                    clocking_activity = cells[i+3].text
                    clocking_origin = cells[i+5].text
                    if (clocking_type != "" and
                        clocking_time != "" and
                        clocking_activity != "" and
                        clocking_origin != ""):
                        clocking = { "type": clocking_type, "time": clocking_time, "activity": clocking_activity, "origin": clocking_origin }
                        clockings.append(clocking)
                    elif (clocking_type == "" and
                          clocking_time == "" and
                          clocking_activity == "" and
                          clocking_origin == ""):
                        break
                    else:
                        logger.error("Unexpected content in clockings, assuming empty")
                        return []
                    # javascript screws these elements so lets keep retrying
                return clockings
            except selenium.common.exceptions.StaleElementReferenceException:
                pass

@click.command()
@click.option("-u","--user", envvar="SRH_USER", required=True, help="username, can be set as env var SRH_USER")
@click.option("-p","--password", envvar="SRH_PASSWORD", required=True, help="password, can be set as env var SRH_PASSWORD")
@click.option("-l","--list-clockings/--no-list-clockings", default=True, help="list current clockings", show_default="activated")
@click.option("-c","--clock/--no-clock", default=False, help="clock in/out the current time (i.e. click the button)", show_default="deactivated")
@click.option("-q", "--headless/--no-headless", default=True, help="headless (no visible browser) operation", show_default="activated")
@click.option("-s", "--site", required=True, envvar="SRH_SITE", help="main URI to log in, can be set as env var SRH_SITE")
@click.option("-t","--timeout", default=120, help="time limit for each browser operation", show_default=True)
@click.option("-r","--retry", default=3, help="number of retries when reaching timeout", show_default=True)
def srh_action(list_clockings,clock,user,password,headless,site,timeout,retry):
    init = datetime.datetime.now()
    if not (list_clockings or clock):
        click.echo("Need either --user or --password")
        click.echo(click.get_current_context().get_help())
        exit()
    global global_timeout
    global global_retries
    global_timeout = timeout
    global_retries = retry
    srh = SRH_Logger(username=user,password=password,headless=headless)
    try:
        srh.go_to_url(site).login()
        if clock:
            srh.clock()
        if list_clockings:
            srh.printClockings()
    except SRHError as e:
        pass
    finally:
        srh.end()
        diff = datetime.datetime.now() - init
        printdelta("Total time saved by srh-logger: ", diff)

if __name__ == "__main__":
    srh_action()


